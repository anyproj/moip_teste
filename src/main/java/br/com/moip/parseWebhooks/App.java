package br.com.moip.parseWebhooks;

import br.com.moip.parseWebhooks.domain.WebhooksBase;

public class App {

    public static void main(String[] args) throws Exception {
        WebhooksBase webhooksBase = new WebhooksBase();
        webhooksBase.run();
        System.out.println("Top 3 urls:");
        System.out.println(webhooksBase.getTop4Urls());
        System.out.println("\nQuantidade de webhooks por status:");
        System.out.println(webhooksBase.getStatusCounters());
    }

}
