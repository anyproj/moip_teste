package br.com.moip.parseWebhooks.domain;

import java.util.*;

class Counter <T> {
    final Map<T, UrlCount> urlCounters    = new HashMap<>();

    public void increment(T obj){
        if(!urlCounters.containsKey(obj)){
            urlCounters.put(obj, new UrlCount(obj, 0));
        }
        urlCounters.put(obj, urlCounters.get(obj).increment());
    }

    public Map<T,Integer> getMostCommon(int quantity){
        final Map<T,Integer> result = new LinkedHashMap<T, Integer>();
        List<UrlCount> counts = new ArrayList<>(urlCounters.values());
        Collections.sort(counts);
        for(int i = 0; i < quantity; i++){
            if(i < counts.size()){
                result.put(counts.get(i).obj,counts.get(i).count);
            }
        }
        return result;
    }

    public Map<T, Integer> getOrderedMap(){
        return getMostCommon(urlCounters.size());
    }

    class UrlCount implements Comparable<UrlCount>{
        final T obj;
        final Integer count;

        public UrlCount(T obj, Integer count) {
            this.obj = obj;
            this.count = count;
        }

        public UrlCount increment(){
            return new UrlCount(obj, count +1);
        }

        @Override
        public int compareTo(UrlCount o) {
            return o.count.compareTo(count);
        }
    }
}