package br.com.moip.parseWebhooks.domain;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WebhooksBase {

    private Counter urlCounter = new Counter<String>();
    private Counter statusCounter = new Counter<Long>();

    public void run() throws IOException {
        loadWebHooks();
    }

    private void loadWebHooks() throws IOException {
        FileReader input = new FileReader("./data/log.txt");
        BufferedReader bufRead = new BufferedReader(input);
        String myLine = null;

        Pattern PatternOne = Pattern.compile("request_to=\"([^\"]+)\".*response_status=\"([^\"]+)\"");

        while ( (myLine = bufRead.readLine()) != null) {
            if(myLine.contains("request_to") && myLine.contains("response_status")){
                Matcher MatcherOne = PatternOne.matcher(myLine);
                if(MatcherOne.find()){
                    String url = MatcherOne.group(1);
                    Long code = Long.valueOf(MatcherOne.group(2));
                    urlCounter.increment(url);
                    statusCounter.increment(code);
                }
            }
        }
    }

    public Map<String, Integer> getTop4Urls(){
        return urlCounter.getMostCommon(3);
    }

    public Map<Long, Integer> getStatusCounters(){
        return statusCounter.getOrderedMap();
    }
}
